# Shape Volume Ranker

Shape Volume Ranker sorts cubes and spheres according to their volumes.

## Running the code:

From the root folder, execute:

```shell
python3 main.py
```
