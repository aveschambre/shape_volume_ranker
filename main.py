import math
import random


def check_color(color):
    '''
    check_color(color) returns False if color is not an hexadecimal
    color. If it is, color is standardized: '#' added if necessary
    and lowcase letters are changed to uppercase for homogeneity.
    '''

    if color[0] == "#":
        color = color[1:]

    if len(color) != 6:
        return False

    hexadec_char = ["0", "1", "2", "3", "4", "5", "6", "7",
                    "8", "9", "A", "B", "C", "D", "E", "F"]
    color = color.upper()

    for elem in color:
        if elem not in hexadec_char:
            return False

    return "#{}".format(color)


def check_length(length):
    '''
    check_length(length) checks if a given
    length is a number greater than zero.
    '''

    if type(length) is int:
        return length > 0
    elif type(length) is float:
        return length > 0
    elif length.isnumeric():            # Case of a user input: length is a str
        return float(length) > 0

    return False


class Shape:
    '''
    Base class for all shapes.
    '''

    def __init__(self, type, color):
        self.type = type

        while not check_color(color):
            print("Oops, the color value '{}' "
                  "is not what I expected.".format(color))
            color = input(
                "Please type a 6 character long hexadecimal value: ")

        self.color = check_color(color)


class Cube(Shape):
    def __init__(self, width, color):
        super().__init__("Cube", color)

        while not check_length(width):
            print("I'm afraid that '{}' cannot "
                  "be the width of a cube.".format(width))
            width = input("Please type a number greater than zero: ")

        self.width = float(width)

    def volume(self):
        return self.width * self.width * self.width


class Sphere(Shape):
    def __init__(self, radius, color):
        super().__init__("Sphere", color)

        while not check_length(radius):
            print("I'm afraid that '{}' cannot be "
                  "the radius of a sphere.".format(radius))
            radius = input("Please type a number greater than zero: ")

        self.radius = float(radius)

    def volume(self):
        return 4.0/3.0 * math.pi * self.radius**3


def min_volume_index(shape_list):
    '''
    min_volume_index(shape_list) returns the index in shape_list of
    the shape with the smaller volume among the shapes listed.
    '''

    min_volume = shape_list[0].volume()
    index = 0

    for i in range(0, len(shape_list)):
        if shape_list[i].volume() < min_volume:
            min_volume = shape_list[i].volume()
            index = i

    return index


def sort_by_volume(shape_list):
    '''
    sort_by_volume(shape_list) sorts the elements of shape_list
    from the smallest volume to the biggest.
    '''

    sorted_list = []

    for i in range(0, len(shape_list)):
        min_index = min_volume_index(shape_list)
        sorted_list.append(shape_list[min_index])
        shape_list.remove(shape_list[min_index])

    return sorted_list


def pretty_printing(shape_list):
    '''
    pretty_printing(shape_list) prints a shape_list in as a table.
    '''

    print(" Shape type         | Volume                 | Color              ")
    print("--------------------+------------------------+--------------------")
    for i in range(0, len(shape_list)):
        string = " " + str(shape_list[i].type)

        for j in range(0, 20 - len(string)):
            string = string + " "

        string = string + "| " + str(round(shape_list[i].volume(), 6))

        for j in range(0, 45 - len(string)):
            string = string + " "

        string = string + "| " + str(shape_list[i].color)
        print(string)


def user_input_mode():
    '''
    user_input_mode() asks the user to provide information about 1 or more
    shapes and creates a shape_list. This list is sorted and printed.
    '''

    shape_list = []
    stop = False

    while not stop:
        type = input("\nType 1 to add a cube, "
                     "2 to add a sphere, 3 to validate: ")

        if type == "1":
            width = input("Type the cube's width: ")
            color = input("Type the cube's color using hexadecimal: ")
            shape_list.append(Cube(width, color))
        elif type == "2":
            radius = input("Type the sphere's radius: ")
            color = input("Type the sphere's color using hexadecimal: ")
            shape_list.append(Sphere(radius, color))
        elif type == "3":
            if len(shape_list) == 0:
                print("\nOh, the shape list is empty. Add at least one!")
            else:
                stop = True
                print("\nAlright. The list contains "
                      "{} shapes.".format(len(shape_list)))
        else:
            print("\nOops, you missed the key! Let's try again.")

    print("\n(*•̀ᴗ•́*)و done ! Here's the shape list sorted by volume:\n")
    pretty_printing(sort_by_volume(shape_list))


def random_length():
    '''
    random_length() returns a random integer.
    '''

    return random.randint(1, 999)


def random_color():
    '''
    random_color() returns a random hexadecimal color.
    '''

    return "%06x" % random.randint(0, 0xFFFFFF)


def auto_example(nb_elem):
    '''
    auto_example(nb_elem) generates a list of nb_elem shapes with
    random characteristics. shape_choice determines if the next
    shape to be created will be a Cube or a Sphere randomly.
    '''

    shape_list = []

    for i in range(0, nb_elem):
        shape_choice = random.choice([True, False])
        if shape_choice:
            shape_list.append(Cube(random_length(), random_color()))
        else:
            shape_list.append(Sphere(random_length(), random_color()))

    pretty_printing(sort_by_volume(shape_list))


def main():
    relaunch = "1"
    skip_input_request = False

    print("⊂(◉‿◉)つ hi")
    print("I rank shapes by their volumes. I understand cubes and spheres.")

    while relaunch != "0":
        if not skip_input_request:
            mode = input("Type 1 to add your own shapes or "
                         "type 2 to see an example: ")

        if mode == "1":
            user_input_mode()
            skip_input_request = False
            relaunch = input("\nPress 0 to quit, anything else to relaunch: ")
        elif mode == "2":
            nb_elem = input("\nGreat, let me do it for you. How many "
                            "shapes would you like me to create and sort? ")
            while not nb_elem.isdigit():
                nb_elem = input("\nఠ_ఠ Please focus and type an integer: ")
            auto_example(int(nb_elem))
            skip_input_request = False
            relaunch = input("\nPress 0 to quit, anything else to relaunch: ")
        else:
            skip_input_request = True
            mode = input("\nᕙ(⇀‸↼‶)ᕗ You made a typo but it's ok. "
                         "Please type 1 or 2: ")
            if mode != "1" and mode != "2":
                print("..my patience is not limitless though.. ''⌐(ಠ۾ಠ)¬''")
                mode = input("I am certified monkey-test-proof. "
                             "Now type 1 or 2: ")
                if mode != "1" and mode != "2":
                    print("༼つಠ益ಠ༽つ you annoyed volume ranker. "
                          "Self-destruction activated.")
                    relaunch = "0"

    print("bye ಠ‿ಠ")

main()
